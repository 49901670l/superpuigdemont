﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaFallingScript : MonoBehaviour {

	public float fallDelay = 1f;
	public float respawnDelay = 3f;
	private Rigidbody2D rbd2; //rigidbody2d plataforma falling
	private PolygonCollider2D pc2d; //polygoncollider2d plataforma falling
	private Vector3 start; //guardar posicio plataforma falling

	// Use this for initialization
	void Start () {
		rbd2 = GetComponent<Rigidbody2D>();
		pc2d = GetComponent<PolygonCollider2D>();
		start = transform.position;
	}

	void Update(){

	}

	//comprovem que el jugador esta a sobre de la plataforma
	void OnCollisionEnter2D(Collision2D col){
		if(col.gameObject.CompareTag("Player")){
			Invoke("Fall", fallDelay); //incovar el fall passat 1 segon
			Invoke("Respawn", fallDelay + respawnDelay);
		}
	}

	//caiguda plataforma
	void Fall(){
		rbd2.isKinematic = false;
		rbd2.gravityScale = 50f;
		pc2d.isTrigger = true;
	}

	//reapareixer plataforma
	void Respawn(){
		transform.position = start;
		rbd2.isKinematic = true;
		rbd2.velocity = Vector3.zero;
		pc2d.isTrigger = false;
	}
}
