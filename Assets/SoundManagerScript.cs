﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour {

	public static AudioClip extraLife, jumpSound, pauseSound, coinSound, stompSound, kickShellSound;
	static AudioSource audioSrc;

	// Use this for initialization
	void Start () {
		
		extraLife = Resources.Load<AudioClip>("extra_life_sound");
		jumpSound = Resources.Load<AudioClip>("jump_sound");
		pauseSound = Resources.Load<AudioClip>("pause_sound");
		coinSound = Resources.Load<AudioClip>("coin_sound");
		stompSound = Resources.Load<AudioClip>("stomp_sound");
		kickShellSound = Resources.Load<AudioClip>("kick_shell_sound");

		audioSrc = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void PlaySound (string clip)
	{
		switch(clip){
		case "extra_life_sound":
			audioSrc.PlayOneShot(extraLife);
			break;
		case "jump_sound":
			audioSrc.PlayOneShot(jumpSound);
			break;
		case "pause_sound":
			audioSrc.PlayOneShot(pauseSound);
			break;
		case "coin_sound":
			audioSrc.PlayOneShot(coinSound);
			break;
		case "stomp_sound":
			audioSrc.PlayOneShot(stompSound);
			break;
		case "kick_shell_sound":
			audioSrc.PlayOneShot(kickShellSound);
			break;
		}
	}
}
