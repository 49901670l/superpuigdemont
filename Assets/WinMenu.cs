﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinMenu : MonoBehaviour {

	bool loadingStarted = false;
    float secondsLeft = 0;

     public GameObject menu; // Assign in inspector
     private bool isShowing;

    void Start()
    {
        StartCoroutine(DelayLoadLevel(15));
    }

    IEnumerator DelayLoadLevel(float seconds)
    {
        secondsLeft = seconds;
        loadingStarted = true;
        do
        {
            yield return new WaitForSeconds(1);
        } while (--secondsLeft > 0);

        isShowing = !isShowing;
        menu.SetActive(isShowing);
    }

    //Tornem al menu principal
	public void LoadMenu()
	{
		Time.timeScale = 1f;
		SceneManager.LoadScene("Menu");
	}
}
