﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class EscenaLogica : MonoBehaviour {
	public PlayerController Player;
	public GameObject[] NivelPreFab;
	public Text Texto;
	private int IndiceDeNiveles;
	private GameObject ObjetoNivel;
	private GameObject moneda;	
	
	// Use this for initialization
	void Start () {
        Texto.text = "Nivel 1";
        IndiceDeNiveles = 0;
		ObjetoNivel = Instantiate(NivelPreFab[IndiceDeNiveles]);
		ObjetoNivel.transform.SetParent(this.transform);
	}
	
	// Update is called once per frame
	void Update () {
        if (Player.SiguienteNivel){
            IndiceDeNiveles = 1;
            Texto.text = "Nivel 2";
            ObjetoNivel = Instantiate(NivelPreFab[IndiceDeNiveles]);
            ObjetoNivel.transform.SetParent(this.transform);
            transform.position = new Vector3(-1, 0, 0);

            Player.SiguienteNivel = false;
		}		
	}
}
