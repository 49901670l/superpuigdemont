﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public static bool GameIsPaused = false;

	//Menu Pause
	public GameObject pauseMenuUI;

	// Update is called once per frame
	void Update () {

		//Comprovem que estem clicant la tecla P
		//en cas afirmatiu, mostra el menu pause
		if(Input.GetKeyDown("p"))
		{
			if(GameIsPaused)
			{
				Resume();
			} else
			{
				Pause();
			}
		}
	}

	//Reanudar el joc
	public void Resume()
	{
		pauseMenuUI.SetActive(false);
		Time.timeScale = 1f;
		GameIsPaused = false;
		SoundManagerScript.PlaySound("pause_sound"); //reproduir efecte de so
	}

	//Parem el joc i mostra el PauseMenu
	void Pause()
	{
		pauseMenuUI.SetActive(true);
		Time.timeScale = 0f;
		GameIsPaused = true;
		SoundManagerScript.PlaySound("pause_sound"); //reproduir efecte de so
	}

	//Tornem al menu principal
	public void LoadMenu()
	{
		Time.timeScale = 1f;
		SceneManager.LoadScene("Menu");
	}

	//Tanquem el joc
	public void QuitGame()
	{
		Application.Quit();
	}
}
