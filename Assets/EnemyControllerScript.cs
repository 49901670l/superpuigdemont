﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControllerScript : MonoBehaviour {

	public float speed = 1f; //velocitat de l'enemic
	public float maxSpeed = 1f; //velocitat maxima de l'enemic
	private Rigidbody2D rbd2; //fisica rigidbody
	
	// Use this for initialization
	void Start () {
		rbd2 = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rbd2.AddForce(Vector2.right * speed); //moviment automatic de l'enemic

		//limitar velocitat enemic
		float limitedSpeed = Mathf.Clamp(rbd2.velocity.x, -maxSpeed, maxSpeed);
		rbd2.velocity = new Vector2(limitedSpeed, rbd2.velocity.y);

		//canviar direccio automaticament de l'enemic quan colisionem
		if(rbd2.velocity.x > -0.01f && rbd2.velocity.x < 0.01f){
			speed = -speed;
			rbd2.velocity = new Vector2(speed, rbd2.velocity.y);
		}
		
		if(speed < 0){
            transform.localScale = new Vector3(31.7458f, 29.98021f, 1f); //girar l'enemic cap a la dreta

        } else if(speed > 0){
            transform.localScale = new Vector3(-31.7458f, 29.98021f, 1f); //girar l'enemic cap a l'esquerra
        }
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.CompareTag("Player")){
			if(transform.position.y < other.transform.position.y){ //comprovar que el personatge esta a sobre de l'enemic
				if(gameObject.tag == "marchena"){
					//other.SendMessage("EnemyJump");
					Destroy(gameObject); //destruir enemic
					SoundManagerScript.PlaySound("kick_shell_sound");
				}

				if(gameObject.tag == "trapote"){
					//other.SendMessage("EnemyJump");
					Destroy(gameObject); //destruir enemic
					SoundManagerScript.PlaySound("stomp_sound");
				}
			} else {
				//other.SendMessage("EnemyKnockBack");
				other.SendMessage("EnemyKnockBack");
			}
		}
	}
}
