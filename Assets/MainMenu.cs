﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	//Quan cliquem el boto PLAY, canviarem a la primera escena
	public void PlayGame(){

		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

	}

	//Quan cliquem al boto QUIT, tanquem el joc
	public void QuitGame()
	{

		Debug.Log("Quit");
		Application.Quit();
	}
}
