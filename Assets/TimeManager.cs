﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeManager : MonoBehaviour {

	public float startingTime;
	private float coutingTime;
	public Text textTime; //Text on mostra el temps
	
	public PlayerController comptadorVides; //comptador de vides del jugador

	void Start()
	{
		coutingTime = startingTime;
	}

	// Update is called once per frame
	void Update () {
		
		coutingTime -= Time.deltaTime; //descomptar temps a cada segon

		//Quan el temporitzador arribi a 0, el jugador perd una vida
		if(coutingTime <= 0)
		{
			SceneManager.LoadScene("SceneLose");
		}

		textTime.text = ""+ Mathf.Round(coutingTime);
	}
}
