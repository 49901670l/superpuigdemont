﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
	
     public float speed = 20f; //velocitat del personatge
     private Animator anim; //animacio del personatge
     private Rigidbody2D rbd2; //fisica rigidbody
     public bool grounded; //comprova si el personatge esta tocant el terra
     public float jumpPower = 10.0f; //força del salt
     private bool jump; //comprova si anem a saltar
     public int count; //comptador de monedes
     public Text text; //mostra les monedes recollides
     public int lifePoints; //vides del personatge
     public Text textLife; //mostra les vides del personatge
     private bool movement = true;
     public bool SiguienteNivel = false;
     private SpriteRenderer spr;

    void Start(){
        
        rbd2 = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        spr = GetComponent<SpriteRenderer>();
        count = 0;
        lifePoints = 3;
        updateCounter();
        updateLifePoints();

        
    }

    void Update(){
        
        anim.SetFloat("Speed", Mathf.Abs(rbd2.velocity.x));
        anim.SetBool("Grounded", grounded);

        if (Input.GetKeyDown(KeyCode.UpArrow) && grounded){
            SoundManagerScript.PlaySound("jump_sound");
            rbd2.velocity = new Vector2(rbd2.velocity.x, jumpPower);
        }

        float horizontalDirection = Input.GetAxisRaw("Horizontal");

        if (horizontalDirection > 0)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        } else if (horizontalDirection < 0)
        {
            transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }

        rbd2.velocity = new Vector2(horizontalDirection * speed, rbd2.velocity.y);

       
    }

    //reapareixer personatge en cas de caiguda
    void OnBecameInvisible(){
        transform.position = new Vector3(-1,0,0);
        lifePoints--;
        updateLifePoints();
        
        //En el cas de que la vida del nostre personatge sigui 0, canviem l'escena 
        if(lifePoints == 0)
        {
            //Anem a Scene de la derrota
             SceneManager.LoadScene("SceneLose");
        }
    }

     //Agafar les monedes
     void OnTriggerEnter2D(Collider2D other)
     {
		//PD: Per que aixo funcioni s'ha danar al prefab de Coin i assegurar-te de que te l'etiqueta Coin
		//i en Collider esta activada l'opcio isTrigger
        if (other.CompareTag("coin"))
        {
            Destroy(other.gameObject);
            count++;

            updateCounter();
            SoundManagerScript.PlaySound("coin_sound");

            //Si recollim un total de 15 monedes, sumem una vida al personatge, i el comptador de monedes es reseteja a zero
            if(count == 15){
                lifePoints++;
                updateLifePoints();
                SoundManagerScript.PlaySound("extra_life_sound");

                count = 0;
                updateCounter();
            }
        }

        //Canviar a nivell 2
        if (other.CompareTag("catlelevel1")){
            //Anem al seguent nivell
            if (lifePoints == 1)
            {
                lifePoints = 3;
                SiguienteNivel = true;
                SceneManager.LoadScene("Level2");
            }
            else
            {
                SiguienteNivel = true;
                SceneManager.LoadScene("Level2");
            }
        }

        
        //Arribar a palau
        if (other.CompareTag("castlelevel2"))
        {
            if (lifePoints == 1)
            {
                lifePoints = 3;
                SceneManager.LoadScene("EndVideo");
            }
            else
            {
                SceneManager.LoadScene("EndVideo");
            }
        }

        //En el cas de que la vida del nostre personatge sigui 0, canviem l'escena 
        if (lifePoints == 0)
        {
            //Anem a Scene de la derrota
             SceneManager.LoadScene("SceneLose");
        }

    }

    //Actualizem el comptador de monedes
    void updateCounter()
    {
        text.text = count+" / 15";
    }

    //Actualitzem el comptador de vides
    void updateLifePoints()
    {
        textLife.text = "x "+lifePoints;
    }

    //salt a l'hora d'eliminar un enemic
    public void EnemyJump(){
        jump = true; 
    }

    /*
    public void EnemyKnockBack(float enemyPosX)
    {
        jump = true;

        //desplaçament del jugador a l'hora de colisionar amb l'enemic
        float side = Mathf.Sign(enemyPosX - transform.position.x);
        rbd2.AddForce(Vector2.left * side * jumpPower, ForceMode2D.Impulse);

        movement = false; //detenir moviment personatge

        Invoke("EnableMovement", 0.7f);

        Color color = new Color(255/255f, 106/255f, 0/255f);
        spr.color = color; //canviar color al impactar amb l'enemic

        lifePoints--;
        updateLifePoints();
    }
    */
    
    public void EnemyKnockBack()
    {
        Color color = new Color(255/255f, 106/255f, 0/255f);
        spr.color = color; //canviar color al impactar amb l'enemic

        Invoke("EnableMovement", 0.7f);

        lifePoints--;
        updateLifePoints();
    }

    void EnableMovement(){
        movement = true;
        spr.color = Color.white; //recuperar el color original
    }
}
